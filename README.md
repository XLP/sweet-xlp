# Sweet XLP

XLP's fork of the Sweet Theme

Including Minor Changes, like:

- Green (instead of Purple) enlargement button
- Blue (instead of Purple) highlighting for selected window in task-manager

# Installation

## Manual

```
# Prepare Folders
sudo mkdir -p /usr/share/color-schemes/
sudo mkdir -p /usr/share/plasma/desktoptheme/
sudo mkdir -p /usr/share/plasma/look-and-feel/
sudo mkdir -p /usr/share/aurorae/themes/
# Install Files
sudo cp files/colors.colors /usr/share/color-schemes/SweetXLP.colors
sudo cp -r files/desktoptheme/ /usr/share/plasma/desktoptheme/SweetXLP/
sudo cp -r files/look-and-feel/ /usr/share/plasma/look-and-feel/SweetXLP/
sudo cp -r files/aurorae/ /usr/share/aurorae/themes/SweetXLP/
```

# Original
Original by [Eliver Lara](https://github.com/EliverLara)

[Sweet KDE Plamsa](https://store.kde.org/p/1286856/)  
[Sweet KDE Colors](https://store.kde.org/p/1294011/)  
[Sweet KDE Aurorae](https://store.kde.org/p/1286856/)
